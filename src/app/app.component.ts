import { Component, OnInit } from '@angular/core';
import { ToastService } from './shared/services/components/toast/toast.service';
import { PrimeNGConfig } from 'primeng/api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'helgen';

  constructor(
    private primengConfig: PrimeNGConfig,
    public toastService: ToastService
  ){}

  ngOnInit() {
    this.primengConfig.ripple = true;
  }
}
