import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { IToast } from 'src/app/core/interfaces/toast.interface';
import { ToastService } from 'src/app/shared/services/components/toast/toast.service';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root'
})

export class InterceptorService implements HttpInterceptor {

  MESSAGES_ERROR_DEFAULT = {
    400: 'Solicitud incorrecta',
    401: 'Sin autenticar: Lo sentimos, ¡Tienes que iniciar sesión para acceder a esto!',
    403: 'Lo sentimos, no puedes acceder a esto!',
    404: 'No pudimos encontrar lo que está buscando. Actualice y vuelva a intentarlo o póngase en contacto con el equipo de asistencia.',
    405: 'No cuenta con los permisos para realizar esta acción',
    422: 'Error de validación',
    500: 'Error del servidor: Comuníquese con el equipo de soporte.',
    504: 'Error del servidor: No pudo completar su solicitud dentro del período de tiempo establecido, comuníquese con el equipo de soporte.'
  };
  MESSAGE_ERROR_GENERAL_DEFAULT = 'Error: actualice e intente nuevamente, o comuníquese con el equipo de soporte.';

  constructor(private toast: ToastService, private authService: AuthService) {

  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const access = localStorage.getItem('access');
    if (access) {
      request = request.clone({
        setHeaders: {
          authorization: `Bearer ${access}`
        }
      });
    }

    return next.handle(request).pipe(
      retry(0),
      catchError(err => {
        const self = this;
        return this.handleError(err, self);
      }));
  }

  handleError(error: HttpErrorResponse, self: any) {
    let message = '';

    if (error.error) {
      console.log(error)
      if ((error.error || error.error.msg)) {
        if (error.error.msg) {
          message = error.error.msg;
        } else {
          message = error.error;
        }
      } else {
        message = this.getErrorMessage(error.status);
      }

      const configAlerta: IToast = {
        key: 'global',
        posicion: 'bottom-center',
        tipo: 'error',
        tiempo: 3000,
        header: '',
        mensaje: message,
        index: 99999
      };

      self.toast.presentarMensaje(configAlerta);
    }

    if (error.error.msg === 'Token has expired') {
      self.authService.logOut();
    }

    return throwError(error);
  }

  getErrorMessage(estatus: string | number) {
    if (estatus && this.MESSAGES_ERROR_DEFAULT[estatus as keyof typeof this.MESSAGES_ERROR_DEFAULT]) {
      return this.MESSAGES_ERROR_DEFAULT[estatus as keyof typeof this.MESSAGES_ERROR_DEFAULT];
    }
    return this.MESSAGE_ERROR_GENERAL_DEFAULT;
  }


}
