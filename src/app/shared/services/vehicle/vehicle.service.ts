import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import { Observable, map } from 'rxjs';
import { Vehicle } from 'src/app/core/models/vehicle.model';

@Injectable({
  providedIn: 'root'
})
export class VehicleService {

  constructor(private api: ApiService) { }

  getVehicles(): Observable<Vehicle[]> {
    const $obs = this.api.lista('vehicle').pipe(
      map((vehicles: Vehicle[]) => vehicles.map((vehicle: Vehicle) => new Vehicle().deserialize(vehicle)))
    );

    return $obs
  }

  getVehicleById(id: string | number): Observable<Vehicle[]> {
    console.log('obteniendo vehiculos por id')
    const $obs = this.api.objeto('vehicle', id);

    $obs.pipe(
      map((vehicles: Vehicle[]) => vehicles.map((vehicle: Vehicle) => {
        return new Vehicle().deserialize(vehicle)
      }))
    );

    return $obs;
  }

}
