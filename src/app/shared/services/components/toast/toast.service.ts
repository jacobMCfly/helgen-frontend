import { Injectable } from '@angular/core';
import { MessageService } from 'primeng/api';
import { IToast } from 'src/app/core/interfaces/toast.interface';

@Injectable({
  providedIn: 'root'
})

export class ToastService {

  toast: IToast = {
    key: '',
    posicion: 'bottom-center',
    header: '',
    mensaje: '',
    tiempo: 3000,
    tipo: '',
    index: 0
  };

  constructor(private messageService: MessageService) {
    this.toast.posicion = 'bottom-center';
    this.toast.header = '';
    this.toast.mensaje = '';
    this.toast.tiempo = 3000;
    this.toast.tipo = '';
  }

  presentarMensaje(config: IToast) {
    this.toast.posicion = config.posicion;
    if (config.index) {
      this.toast.index = config.index;
    } else {
      this.toast.index = 0;
    }
    this.messageService.add({
      key: config.key,
      severity: config.tipo,
      summary: config.header,
      detail: config.mensaje,
      life: config.tiempo,
      closable: config.btnCerrar
    });
  }

  cerrar() {
    this.messageService.clear();
  }

}
