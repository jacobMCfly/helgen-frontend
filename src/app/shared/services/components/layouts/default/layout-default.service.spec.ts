import { TestBed } from '@angular/core/testing';

import { LayoutDefaultService } from './layout-default.service';

describe('LayoutDefaultService', () => {
  let service: LayoutDefaultService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LayoutDefaultService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
