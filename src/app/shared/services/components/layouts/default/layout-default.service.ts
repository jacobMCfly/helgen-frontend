import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

export interface AppConfig {
  colorScheme: string;
  ripple: boolean;
  menuMode: string;
  scale: number;
}

@Injectable({
  providedIn: 'root',
})
export class LayoutDefaultService {

  private overlayOpenSubject: BehaviorSubject<boolean>;
  public overlayOpen: Observable<boolean>


  constructor() {
    this.overlayOpenSubject = new BehaviorSubject<boolean>(false);
    this.overlayOpen = this.overlayOpenSubject.asObservable();
  }

  public get toggle() {
    return this.overlayOpenSubject.value;
  }

  onMenuToggle() {
    console.log('activando el menú')
    this.overlayOpenSubject.next(!this.overlayOpenSubject.value);
  }

  isDesktop() {
    return window.innerWidth > 991;
  }

  isMobile() {
    return !this.isDesktop();
  }

  unsubscribe() {
    this.overlayOpenSubject.next(false);
    this.overlayOpenSubject.complete();
  }


}
