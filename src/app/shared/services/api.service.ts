import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  webApi = environment.server;
  headers: HttpHeaders | undefined;

  private constructor(private http: HttpClient) {
  }

  generarQueryParams(params: object | string) {
    params = Object.entries(params)
      .map(([key, value]) => `${key}=${value}`)
      .join('&');
    return params ? `?${params}` : '';
  };

  lista(modelo: string, filtro?: object) {
    const queryParams = this.generarQueryParams(filtro || {});
    return this.http.get<any>(`${this.webApi}/${modelo}/${queryParams}`);
  }

  objeto(modelo: string, id: string | number) {
    return this.http.get<any>(`${this.webApi}/${modelo}/${id}/`);
  }

  guardar(modelo: string, obj: object) {
    return this.http.post<any>(`${this.webApi}/${modelo}/`, obj);
  }

  actualizar(modelo: string, obj: any) {
    return this.http.patch<any>(`${this.webApi}/${modelo}/${obj.id}/`, obj);
  }

  actualizarConFormData(modelo: string, form: any, obj: any) {
    return this.http.patch<any>(`${this.webApi}/${modelo}/${obj.id}/`, form);
  }

  eliminar(modelo: string, obj: any) {
    return this.http.delete<any>(`${this.webApi}/${modelo}/${obj.id}/`);
  }

  get(endPont: string) {
    return this.http.get<any>(`${this.webApi}/${endPont}/`);
  }

  getUrlCompleta(url: string) {
    return this.http.get<any>(`${url}`);
  }

  post(endPoint: string, obj: object) {
    return this.http.post<any>(`${this.webApi}/${endPoint}/`, obj);
  }

}
