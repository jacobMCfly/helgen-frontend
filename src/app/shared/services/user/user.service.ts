import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import { BehaviorSubject, Observable, map } from 'rxjs';
import { User } from 'src/app/core/models/user.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private userSubject: BehaviorSubject<User | null>;
  public user: Observable<User | null>;

  constructor(private api: ApiService) {
    this.userSubject = new BehaviorSubject(JSON.parse(localStorage.getItem('user')!));
    this.user = this.userSubject.asObservable();
  }

  public get userValue() {
    return this.userSubject.getValue();
  }

  setUserData(): Observable<User> {
    const $obs = this.api.get('user/logged').pipe(
      map((user) => {
        const obj = JSON.parse(JSON.stringify(user));
        localStorage.setItem('user', JSON.stringify(user));
        this.userSubject.next(obj);
        return obj;
      }
      )
    );

    return $obs
  }

  removeUserData() {
    this.userSubject.next(null);
  }

  unsubscribe() {
    this.userSubject.complete();
  }



}
