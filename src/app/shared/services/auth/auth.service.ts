import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import { IToken } from 'src/app/core/interfaces/token.interface';
import { environment } from 'src/environments/environment';
import { Observable, map } from 'rxjs';
import { Router } from '@angular/router';
import { User } from 'src/app/core/models/user.model';
import { UserService } from '../user/user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  webApi = environment.server;

  constructor(
    private api: ApiService,
    private router: Router,
    private userService: UserService) { }

  login(email: string, password: string): Observable<boolean> {
    return this.getToken(email, password).pipe(
      map(logged => {
        if (logged) {
          return true;
        }
        return false;
      })
    );
  }

  getToken(email: string, password: string): Observable<boolean> {
    return this.api.post('login', {
      email,
      password
    }).pipe(
      map(token => {
        if (token) {
          this.saveToken(token);
          return true;
        }
        return false;
      })
    );
  }

  saveToken(token: IToken) {
    localStorage.setItem('access', token.access_token);
  }

  logOut() {
    localStorage.clear();
    this.userService.removeUserData();
    this.router.navigate(['/login']);
  }
}
