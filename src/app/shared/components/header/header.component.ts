import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { LayoutDefaultService } from '../../services/components/layouts/default/layout-default.service';
import { ConfirmationService, MenuItem } from 'primeng/api';
import { AuthService } from '../../services/auth/auth.service';
import { User } from 'src/app/core/models/user.model';
import { UserService } from '../../services/user/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  items!: MenuItem[];
  user!: User;

  constructor(
    public layoutDefaultService: LayoutDefaultService,
    public confirmationService: ConfirmationService,
    private authService: AuthService,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.user = new User().deserialize(this.userService.userValue);

    this.items = [
      {
        icon: 'pi pi-home',
        label: 'Home',
        routerLink: ['/home'],
        visible: true,

      },
      {
        label: 'Routing',
        icon: 'pi pi-map',
        routerLink: ['/map'],
        visible: this.user.getRole === 'operator' || this.user.getRole === 'superuser' ? true : false,
      },
      {
        icon: 'pi pi-ellipsis-h',
        command: () => this.layoutDefaultService.onMenuToggle()
      }
    ];
  }

  logOut() {
    this.confirmationService.confirm({
      key: 'logOut',
      header: 'Attention',
      message: '¿Are you sure you want to log out??',
      dismissableMask: false,
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.authService.logOut();
      }
    });
  }
}
