import { Component, OnInit } from '@angular/core';
import { LayoutDefaultService } from '../../services/components/layouts/default/layout-default.service';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {

  display = false;
  items!: MenuItem[];

  constructor(public layoutDefaultService: LayoutDefaultService) { }

  ngOnInit(): void {
    this.layoutDefaultService.overlayOpen.subscribe(data => {
      this.display = data;
    });


  }

  sidenavClosed() {
    this.layoutDefaultService.onMenuToggle();
  }

  ngOnDestroy(): void {
    this.layoutDefaultService.unsubscribe();
  }

}
