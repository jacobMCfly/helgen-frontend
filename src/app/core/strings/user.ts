export const TEXT = {
  superuser: {
    sp: {
      welcome: 'Bienvenido',
      err: {
        permissions: ''
      }
    },
    en: {
      welcome: 'Welcome',
      err: {
        permissions: ''
      }
    },

  },
  user: {
    sp: {
      welcome: 'Bienvenido',
      err: {
        permissions: 'Lo sentimos, no cuenta con los permisos para visualizar las rutas'
      }
    },
    en: {
      welcome: 'Welcome',
      err: {
        permissions: 'Sorry, you do not have the permissions to view the routes'
      }
    }
  },
  operator: {
    sp: {
      welcome: 'Bienvenido',
      err: {
        permissions: ''
      }
    },
    en: {
      welcome: 'Welcome',
      err: {
        permissions: 'Welcome you can navigate to the map from the menu above'
      }
    }
  }
}
