import { Deserializable } from "src/app/shared/models/deserealizable.model";

export class Vehicle implements Deserializable {

  private id!: number;
  private DataEntryID!: number;
  private Date!: string;
  private Latitude!: number;
  private Longitude!: number;
  private Time!: string;
  private VehicleID!: number;

  deserialize(input: any) {
    Object.assign(this, input);
    return this;
  }

  public get getId(): number {
    return this.id;
  }

  public get getDataEntryID(): number {
    return this.DataEntryID;
  }

  public get getDate(): string {
    return this.Date;
  }

  public get getLatitude(): number {
    return this.Latitude;
  }

  public get getLongitude(): number {
    return this.Longitude;
  }

  public get getTime(): string {
    return this.Time;
  }

  public get getVehicleID(): number {
    return this.VehicleID;
  }

}
