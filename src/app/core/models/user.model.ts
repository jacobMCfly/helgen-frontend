import { Deserializable } from "src/app/shared/models/deserealizable.model";

export class User implements Deserializable {

  private id!: string | number;
  private email!: string;
  private first_name!: string;
  private last_name!: string;
  private role!: string;

  deserialize(input: any) {
    Object.assign(this, input);
    return this;
  }

  get getRole(): string {
    return this.role;
  }

  get getEmail(): string {
    return this.email;
  }

  get getName(): string {
    return this.first_name + ' ' + this.last_name;
  }


}
