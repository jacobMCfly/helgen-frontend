export interface IToast {
  posicion: string;
  key: string;
  tipo: string;
  header: string;
  mensaje: string;
  tiempo?: number;
  btnCerrar?: boolean;
  index: number;
}
