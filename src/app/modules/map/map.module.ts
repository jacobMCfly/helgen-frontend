import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MapRoutingModule } from './map-routing.module';
import { MapPageComponent } from './pages/map-page/map-page.component';
import { DropdownModule } from 'primeng/dropdown';
import { CardModule } from 'primeng/card';

@NgModule({
  declarations: [
    MapPageComponent
  ],
  imports: [
    CommonModule,
    MapRoutingModule,
    DropdownModule,
    CardModule
  ]
})
export class MapModule { }
