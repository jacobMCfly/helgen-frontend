import { Component, ElementRef, OnInit, ViewChild, Renderer2 } from '@angular/core';
import { Subscription } from 'rxjs';
import { Vehicle } from 'src/app/core/models/vehicle.model';
import { GmapService } from 'src/app/shared/services/gmap/gmap.service';
import { VehicleService } from 'src/app/shared/services/vehicle/vehicle.service';
// import { MarkerClusterer } from '@googlemaps/markerclusterer';
declare var MarkerClusterer: any;

@Component({
  selector: 'app-map-page',
  templateUrl: './map-page.component.html',
  styleUrls: ['./map-page.component.scss']
})
export class MapPageComponent implements OnInit {

  @ViewChild('map', { static: true }) mapElementRef!: ElementRef;
  source: google.maps.LatLngLiteral = { lat: 53.3323073, lng: -6.2723535 };
  map!: google.maps.Map;
  googleMapsOptions: google.maps.MapOptions = {
    center: this.source,
    disableDefaultUI: true,
    zoom: 12
  };
  markerClusterVehicles: any;
  markerVehicles: any[];
  infoWindow: any;

  vehicles!: Vehicle[];
  vehiclesSub$!: Subscription;

  vehicleRouteById!: Vehicle[];
  vehicleRouteByIdSub$!: Subscription;

  vehicleSelected!: Vehicle;

  constructor(
    private renderer: Renderer2,
    private maps: GmapService,
    private vehicleService: VehicleService
  ) {
    this.markerVehicles = [];
  }

  ngOnInit(): void {
    this.getVehicles();
  }

  ngAfterViewInit(): void {
    this.loadMap();
  }

  ngOnDestroy(): void {
    this.vehiclesSub$.unsubscribe();
    if (this.vehicleRouteByIdSub$) {
      this.vehicleRouteByIdSub$.unsubscribe();
    }
  }

  async loadMap(): Promise<void> {
    try {
      await this.maps.loadGoogleMaps();
      const mapElement = this.mapElementRef.nativeElement;
      this.map = new google.maps.Map(mapElement, this.googleMapsOptions);

      const sourcePosition = new google.maps.LatLng(this.source);
      this.map.setCenter(sourcePosition);
      this.renderer.addClass(mapElement, 'visible');
    } catch (error) {
      console.log(error);
    }
  }

  getVehicles(): void {
    this.vehiclesSub$ = this.vehicleService.getVehicles().subscribe((vehicles: Vehicle[]) => {
      this.vehicles = vehicles;
    });
  }

  getVehicleById(VehicleID: string | number): void {
    this.vehicleRouteByIdSub$ = this.vehicleService.getVehicleById(VehicleID).subscribe((vehicle: Vehicle[]) => {
      this.vehicleRouteById = vehicle;
      if (this.vehicleRouteById) {
        this.drawInMap(this.vehicleRouteById);
      }
    });
  }

  selectVehicle($event: any): void {
    if ($event.value === null) {
      const sourcePosition = new google.maps.LatLng(this.source);
      this.map.setCenter(sourcePosition);
      if (this.markerClusterVehicles) {
        this.markerClusterVehicles.clearMarkers();
        for (var i = 0; i < this.markerVehicles.length; i++) {
          this.markerVehicles[i].setMap(null);
        }
        this.markerVehicles.length = 0;
      }
      return;
    }
    this.vehicleSelected = new Vehicle().deserialize($event.value);
    this.getVehicleById(this.vehicleSelected.getVehicleID);
  }

  drawInMap(VehicleData: Vehicle[]): void {
    this.clearMap();

    // order by data entry
    const orderByDataEntry = VehicleData.sort((n1, n2) => n1.getVehicleID - n2.getVehicleID);

    const image = {
      url: 'assets/marker/location_truck.png',
      scaledSize: new google.maps.Size(50, 50)
    };



    const dateOptions = {
      weekday: 'long',
      year: 'numeric',
      month: 'long',
      day: 'numeric',
      hour: '2-digit',
      minute: '2-digit',
      hour12: true
    };

    let vehiclesDataMarker = [];
    for (const [index, val] of orderByDataEntry.entries()) {
      const element = new Vehicle().deserialize(val);
      let array = [];

      array[0] = element.getVehicleID;
      array[1] = element.getLatitude;
      array[2] = element.getLongitude;
      array[3] = index + 1;
      const date = new Date(element.getDate).toLocaleDateString('en-US', dateOptions as any);
      array[4] = `
        <strong> Vehicle id:  ${element.getVehicleID} </strong><br>
        Number: ${index + 1} <br>
        DataEntryID: ${element.getDataEntryID} <br>
        Date: ${date} <br>
        Time: ${element.getTime}`;

      vehiclesDataMarker.push(array);
    }

    for (let i = 0; i < vehiclesDataMarker.length; i++) {
      let vehicleDataMarker = vehiclesDataMarker[i];
      let marker: google.maps.Marker;
      let numberMarker = i + 1;
      let markerStr = numberMarker.toString();

      let bounds = new google.maps.LatLngBounds();

      let coordenadas = { lat: vehicleDataMarker[1], lng: vehicleDataMarker[2] } as google.maps.LatLngLiteral;
      bounds.extend(coordenadas);
      marker = new google.maps.Marker({
        position: bounds.getCenter(),
        // map,
        icon: image,
        title: String(vehicleDataMarker[0]),
        zIndex: Number(vehicleDataMarker[3]),
        label: {
          text: markerStr,
          color: 'red',
          fontSize: "16px",
          fontWeight: "bold"
        },
        optimized: false,
        visible: true,
        draggable: false,
      });

      this.markerVehicles.push(marker);

      let infoWindow = new google.maps.InfoWindow({
        content: String(vehicleDataMarker[4]),
        maxWidth: 400,
      });

      this.markerVehicles[i].addListener('click', () => {
        infoWindow.open(this.map, this.markerVehicles[i]);
      });
    }

    let bounds = new google.maps.LatLngBounds();

    for (let index = 0; index < vehiclesDataMarker.length; index++) {
      const element = vehiclesDataMarker[index] as any;

      const point = new google.maps.LatLng(element[1], element[2]);
      bounds.extend(point);
    }

    this.map.fitBounds(bounds);
    this.markerClusterVehicles = new MarkerClusterer(this.map, this.markerVehicles, { imagePath: 'assets/marker/m', maxZoom: 16 });

  }


  clearMap(): void {
    if (this.markerClusterVehicles) {
      this.markerClusterVehicles.clearMarkers();
      for (var i = 0; i < this.markerVehicles.length; i++) {
        this.markerVehicles[i].setMap(null);
      }
      this.markerVehicles.length = 0;
    }
  }



}
