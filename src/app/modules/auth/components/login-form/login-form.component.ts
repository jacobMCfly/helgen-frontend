import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first, mergeMap } from 'rxjs';
import { IToast } from 'src/app/core/interfaces/toast.interface';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import { ToastService } from 'src/app/shared/services/components/toast/toast.service';
import { UserService } from 'src/app/shared/services/user/user.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {

  form!: FormGroup;
  loading = false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private toast: ToastService,
    private userService: UserService
  ) {

  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      email: ['', Validators.compose([
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'),
        Validators.required
      ])],
      password: ['', Validators.required]
    });
  }

  get formControls() { return this.form.controls; }

  onSubmitLoggin() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.form.invalid) {
      const configAlerta: IToast = {
        key: 'global',
        posicion: 'bottom-center',
        tipo: 'error',
        tiempo: 3000,
        header: '',
        mensaje: 'Invalid form',
        index: 99999
      };
      this.toast.presentarMensaje(configAlerta);
      return;
    }

    this.authService.login(this.formControls['email'].value, this.formControls['password'].value)
      .pipe(
        first(),
        mergeMap(() => this.userService.setUserData())
      )
      .subscribe({
        next: () => {
          this.router.navigateByUrl('/home');
        },
        error: error => {
          console.log(error);
        }
      });
  }

}
