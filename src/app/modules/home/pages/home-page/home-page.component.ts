import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/core/models/user.model';
import { UserService } from 'src/app/shared/services/user/user.service';
import { TEXT } from 'src/app/core/strings/user';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  user!: User;
  text: any = TEXT;
  message!: string;

  constructor(public userService: UserService) { }

  ngOnInit(): void {
    this.user = new User().deserialize(this.userService.userValue);
  }

}
