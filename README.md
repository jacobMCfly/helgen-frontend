# Helgen Bus Tracker

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.2.6.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Instructions 

To run this project it is necessary to run the backend REST service that is developed in the following link: https://gitlab.com/jacobMCfly/helgen-backend
once the project is running, login with the following users 

**users:**

email = operator@asd.com
password = qweasdfg

email = superuser@asd.com
password = qweasdfg

email = user@asd.com
password = qweasdfg

The user operator and super user is the only one with the permissions.

The project has JWT authentication, use of guards, models, services, layout, components, PRIMENG library.

## IMAGENES

![](./resources/1.png)
![](./resources/2.png)
![](./resources/3.png)
![](./resources/4.png)
![](./resources/5.png)
![](./resources/6.png)
![](./resources/7.png)




 
